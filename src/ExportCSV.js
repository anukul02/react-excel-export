import React from 'react';

import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';

export const ExportCSV = ({ csvData, fileName }) => {
  const fileType =
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  const fileExtension = '.xlsx';

  const exportToCSV = (csvData, fileName) => {
    const ws = XLSX.utils.json_to_sheet(csvData[0]);
    const ws1 = XLSX.utils.json_to_sheet(csvData[1]);
    const ws2 = XLSX.utils.json_to_sheet(csvData[2]);

    const wb = {
      Sheets: {
        Summary: ws,
        'Early Settlement': ws1,
        'Outstanding Settlement': ws2,
      },
      SheetNames: ['Summary', 'Early Settlement', 'Outstanding Settlement'],
    };

    const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

    const data = new Blob([excelBuffer], { type: fileType });

    FileSaver.saveAs(data, fileName + fileExtension);
  };

  return (
    <button onClick={(e) => exportToCSV(csvData, fileName)}>Export</button>
  );
};
